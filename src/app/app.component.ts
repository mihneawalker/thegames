import { Component } from '@angular/core';
import { DataService } from './services/data.service';
import { debounceTime, switchMap } from 'rxjs/operators';
import { BehaviorSubject, combineLatest } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private currentPage = 1;
  currentPage$ = new BehaviorSubject<number>(this.currentPage);
  private searchText$ = new BehaviorSubject<string>('');

  games$ = combineLatest(
      this.searchText$,
      this.currentPage$
  ).pipe(
      debounceTime(150),
      switchMap(([searchText, currentPage] ) => this.dataService.getGames$(currentPage, searchText ))
  );

  constructor(private dataService: DataService) {}

  getSearchValue(searchText) {
    console.log('SEARCH TEXT', searchText);
    this.searchText$.next(searchText);
    this.currentPage = 1;
    this.currentPage$.next(this.currentPage);
  }

  nextPage() {
    this.currentPage++;
    this.currentPage$.next(this.currentPage);
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
    } else {
      this.currentPage = 1;
    }
    this.currentPage$.next(this.currentPage);
  }
}
