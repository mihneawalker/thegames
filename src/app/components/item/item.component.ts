import { Component, Input } from '@angular/core';
import { Game } from '../../model/game.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  @Input() game: Game;

  constructor() { }


}
