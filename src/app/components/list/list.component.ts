import { Component, Input } from '@angular/core';
import { Game } from '../../model/game.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {
  @Input() games: Game[];

  constructor() { }

}
