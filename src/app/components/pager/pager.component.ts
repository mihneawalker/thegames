import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.scss']
})
export class PagerComponent {

  @Output() nextPage = new EventEmitter();
  @Output() previousPage = new EventEmitter();

  constructor() { }


  onPrevious() {
    this.previousPage.emit();
  }

  onNext() {
    this.nextPage.emit();
  }

}
