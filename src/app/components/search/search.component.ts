import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @Output() searchModified = new EventEmitter<string>();
  search = new FormControl('');

  constructor() { }

  onFilter(event) {
    this.searchModified.emit(this.search.value);
  }

}
