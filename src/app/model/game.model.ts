export class Game {
  slug: string;
  name: string;
  shortName: string;
  vendor: string;
  description: string;
  newGame: boolean;
  popularity: number;
  categories: Array<string>;
  tags: Array<string>;

  constructor(slug: string,
              name: string,
              shortName: string,
              vendor: string,
              description: string,
              newGame: boolean,
              popularity: number,
              categories: Array<string>,
              tags: Array<string>) {
    this.slug = slug;
    this.name = name;
    this.shortName = shortName;
    this.vendor = vendor;
    this.description = description;
    this.newGame = newGame;
    this.popularity = popularity;
    categories = this.categories;
    tags = this.tags;
  }
}
