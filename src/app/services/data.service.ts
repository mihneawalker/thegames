import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GameInterface } from './game.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseURL = 'http://localhost:3000/games';

  constructor(private httpService: HttpClient) { }

  getGames$(page: number, query?: string): Observable<GameInterface[]> {
    const options = {
      params: new HttpParams()
        .set('_page', page.toString())
        .set('_limit', '8')
        .set('q', query)
    };
    return this.httpService.get<GameInterface[]>(this.baseURL, options);
  }
}
