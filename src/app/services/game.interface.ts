export interface GameInterface {
  slug: string;
  name: string;
  shortName: string;
  vendor: string;
  description: string;
  newGame: boolean;
  popularity: number;
  categories: Array<string>;
  tags: Array<string>;
}
